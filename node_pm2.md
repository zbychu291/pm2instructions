<i><h1>Instalacja projektu Node JS na PM2</h1></i>

<ol>
<li>Instalacja i zbudowanie wszystkich api w katalogach (eta,ems,candle)
<pre>
npm i
npm run build
</pre>
<li>Utworzenie procesu ETA (w katalogu crypto_api/eta)

<pre>pm2 start 'npm run start:node' --name ETA</pre>

<li>Utworzenie procesu CANDLE (w katalogu crypto_api/candle)

<pre>pm2 start 'npm run start:node' --name CANDLE</pre>

<li>Utworzenie procesu EMS (w katalogu crypto_api/ems)

<pre>pm2 start 'npm start' --name EMS</pre>
<li>Zapisanie procesow do pliku startowego

<pre>pm2 save</pre>


</ol>