<h1>Pierwsze kroki</h1>
<h3>Instalacja PM2 i MongoDB</h3>
<ol>
<li>Instalacja NVM na serwerze/dockerze

https://github.com/nvm-sh/nvm#installing-and-updating
<li>Instalacja MongoDB

https://techviewleo.com/how-to-install-and-use-mongodb-in-linux-mint/
<li>Instalacja PM2

<pre>npm install -g pm2</pre>

<li>Utworzenie autostartu PM2
    
<pre>
npm startup 

[PM2] You have to run this command as root. Execute the following command: 
sudo su -c "env PATH=$PATH:/home/unitech/.nvm/versions/node/v14.3/bin pm2 startup 'distribution' -u 'user' --hp 'home-path'
</pre>
Skopiuj i wykonaj wyświetloną w terminalu komendę 'sudo su - c "env PATH"...'
</li>

    
</ol>