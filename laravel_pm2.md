<i><h1>Instalacja projektu Laravel na PM2</h1></i>

<ol>
<li>Zbuduj projekt Laravel</li>
<li>Utworzyc plik ecosystem.config.json do folderu root projektu Laravel:
    <pre>
        {
        "apps": [{    
            "name": "laravel-app",
            "script": "artisan",
            "args": ["serve", "--host=0.0.0.0", "--port=3333"],
            "instances": "1",
            "wait_ready": true,
            "autorestart": false,
            "max_restarts": 1,
            "interpreter" : "php",
            "watch": true,
            "error_file": "log/err.log",
            "out_file": "log/out.log",
            "log_file": "log/combined.log",
            "time": true
            }]
        }
    </pre>

<li>Utworzenie procesu (root folder Laravel)

<pre>pm2 start ecosystem.config.json</pre>

<li>Zapisanie procesow do pliku startowego
<pre>pm2 save</pre>


</ol>